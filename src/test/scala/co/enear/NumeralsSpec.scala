package co.enear

import org.specs2._
import org.specs2.matcher.Matchers

// Change this spec to whatever you think is suitable to cover important or all cases
class NumeralsSpec extends mutable.Specification with Matchers {
  "Numerals object should" should {
    "Compute the integer value of the " in {
      "I numeral" in Numerals.numeralsToInteger("I") === 1
      "II numeral" in Numerals.numeralsToInteger("II") === 2
      "IV numeral" in Numerals.numeralsToInteger("IV") === 4
      "VIII numeral" in Numerals.numeralsToInteger("VIII") === 8
      "IX numeral" in Numerals.numeralsToInteger("IX") === 9
      "XI numeral" in Numerals.numeralsToInteger("XI") === 11
      "XLIV numeral" in Numerals.numeralsToInteger("XLIV") === 44
      "XCIII numeral" in Numerals.numeralsToInteger("XCIII") === 93
      "CXLIX numeral" in Numerals.numeralsToInteger("CXLIX") === 149
      "CMXCIX numeral" in Numerals.numeralsToInteger("CMXCIX") === 999
      "MCXLIX numeral" in Numerals.numeralsToInteger("MCXLIX") === 1149
      "MCDXC numeral" in Numerals.numeralsToInteger("MCDXC") === 1490
      "MMXIV numeral" in Numerals.numeralsToInteger("MMXIV") === 2014
      "MMLI numeral" in Numerals.numeralsToInteger("MMLI") === 2051
      "MMDCCXCVIII numeral" in Numerals.numeralsToInteger("MMDCCXCVIII") === 2798
    }
  }
}
