
package co.enear

object Numerals {

  /**
   * Given a string containing a correct roman Numeral, returns its integer value
   *
   */
  def numeralsToInteger(numerals: String): Int = {
    numerals match {
      case n if n.startsWith("M") => 1000 + numeralsToInteger(n.substring(1))

      case n if n.startsWith("D") => 500 + numeralsToInteger(n.substring(1))

      case n if n.startsWith("CM") => 900 + numeralsToInteger(n.substring(2))
      case n if n.startsWith("CD") => 400 + numeralsToInteger(n.substring(2))
      case n if n.startsWith("C") => 100 + numeralsToInteger(n.substring(1))


      case n if n.startsWith("L") => 50 + numeralsToInteger(n.substring(1))

      case n if n.startsWith("XC") => 90 + numeralsToInteger(n.substring(2))
      case n if n.startsWith("XL") => 40 + numeralsToInteger(n.substring(2))
      case n if n.startsWith("X") => 10 + numeralsToInteger(n.substring(1))

      case n if n.startsWith("V") => 5 + numeralsToInteger(n.substring(1))

      case n if n.startsWith("IX") => 9 + numeralsToInteger(n.substring(2))
      case n if n.startsWith("IV") => 4 + numeralsToInteger(n.substring(2))
      case n if n.startsWith("I") => 1 + numeralsToInteger(n.substring(1))
      case _ => 0
    }
  }
}
